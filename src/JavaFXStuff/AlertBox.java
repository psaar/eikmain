package JavaFXStuff;

import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.stage.*;
import javafx.geometry.*;

public class AlertBox {

	static boolean vastus;

	public static boolean display(String title, String message) {
		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL); //aken k�ige ees, ei saa vahatada	
		window.setTitle(title);
		window.setMinWidth(250);
		
		Label label = new Label();
		label.setText(message);
		
		Button eiNupp = new Button("Ei");
		eiNupp.setOnAction(e -> {
			vastus = false;
			window.close();
		});
		Button jahNupp = new Button("Jah");
		jahNupp.setOnAction(e -> {
			vastus = true;
			window.close();
		});
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label, jahNupp, eiNupp);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
		
		return vastus;
	}
}
