package JavaFXStuff;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class CloseApp extends Application {

    Stage window;
    Button button;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        window = primaryStage;
        window.setTitle("JavaFX - thenewboston");
        
        window.setOnCloseRequest(e -> {
        	e.consume();	//cancel previous request and continue
        	closeProgram();
        });
        
        button = new Button("Click Me");
        button.setOnAction(e -> closeProgram());

        StackPane layout = new StackPane();
        layout.getChildren().add(button);
        Scene scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();
    }
    
    private void closeProgram() {
    	boolean vastus = AlertBox.display("Title", "kindel et tahad sulgeda");
    	if(vastus) {
    		window.close();
    	}
    }
}