package JavaFXStuff;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Lambdas extends Application {

    Button button;
    
    //Seadistab programmi JavaFX Application klassi j�rgi
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    	
        primaryStage.setTitle("Random Number Generator");
        button = new Button();
        button.setText("Click me");
        button.setOnAction(e -> {
        	System.out.println("lambda expressions");
        	System.out.println("rida 2");
        });
        
        StackPane layout = new StackPane();
        layout.getChildren().add(button);
        
        Scene scene = new Scene(layout, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    public int random () {
    	int min = 0;
        int max = 0;

        return (int) (min + Math.random() * (max - min + 1));
    }
}