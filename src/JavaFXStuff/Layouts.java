package JavaFXStuff;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

	public class Layouts extends Application {

		Stage window;
		
	    public static void main(String[] args) {
	        launch(args);
	    }

	    @Override
	    public void start(Stage primaryStage) throws Exception {
	    	
	        window = primaryStage;
	        window.setTitle("layout stuff");
	        
	        HBox topMenu = new HBox(); 		//Important, do not use HBox for topmenus EVER!
	        Button topA = new Button("File");
	        Button topB = new Button("Edit");
	        Button topC = new Button("View");
	        topMenu.getChildren().addAll(topA, topB, topC);
	        
	        VBox leftMenu = new VBox();
	        Button leftA = new Button("leftA");
	        Button leftB = new Button("leftB");
	        Button leftC = new Button("leftC");
	        leftMenu.getChildren().addAll(leftA, leftB, leftC);
	        
	        BorderPane borderPane = new BorderPane();
	        borderPane.setTop(topMenu);
	        borderPane.setLeft(leftMenu);
	        
	        Scene scene = new Scene(borderPane, 300, 250);
	        window.setScene(scene);
	        window.show();
	    }
	}