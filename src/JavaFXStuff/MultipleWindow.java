package JavaFXStuff;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class MultipleWindow extends Application {

	Stage window;
	Button button;
	
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		window = primaryStage;
		window.setTitle("its a title");
		
		button = new Button("Click me");
		button.setOnAction(e -> {
			boolean arvuVahetus = AlertBox.display("T�helepanu", "Piirkonna algus peab olema v�iksem piirkonna l�pust.");
			System.out.println(arvuVahetus);
		});
		
		StackPane layout = new StackPane();
		layout.getChildren().add(button);
		Scene scene = new Scene(layout, 300, 250);
		
		
		window.setScene(scene);
		window.show();
	}
}