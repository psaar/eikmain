package JavaFXStuff;


import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TableMain extends Application {

    Stage window;
    TableView<TableProducts> table;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("table");

        //Name column
        TableColumn<TableProducts, String> areaColumn = new TableColumn<>("Area");
        areaColumn.setMinWidth(200);
        areaColumn.setCellValueFactory(new PropertyValueFactory<>("area"));

        //Quantity column
        TableColumn<TableProducts, String> timeColumn = new TableColumn<>("Time");
        timeColumn.setMinWidth(100);
        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));

        table = new TableView<>();
        table.setItems(getResult());
        table.getColumns().addAll(areaColumn, timeColumn);

        VBox searchTable = new VBox();
        searchTable.getChildren().addAll(table);

        Scene scene = new Scene(searchTable);
        window.setScene(scene);
        window.show();
    }

    //Get all of the products
    public ObservableList<TableProducts> getResult(){
        ObservableList<TableProducts> areas = FXCollections.observableArrayList();
        areas.add(new TableProducts("Laptop", 20));
        return areas;
    }


}