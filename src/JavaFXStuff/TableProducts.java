package JavaFXStuff;

public class TableProducts{

    private String area;
    private int time;

    public TableProducts(){
        this.area = "";
        this.time = 0;
    }

    public TableProducts(String area, int time){
        this.area = area;
        this.time = time;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

}