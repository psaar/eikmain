package Kontrolltoo2;

/**
 * Created by mmuru on 4.11.15.
 */
/*
Koostage Java meetod etteantud täisarvumaatriksi m veerusummade massiivi leidmiseks
(massiivi j-s element on maatriksi j-nda veeru summa). Arvestage, et m read võivad olla erineva pikkusega.
 */
public class ColumnSums {

    public static void main(String[] args) {
        int[] res = veeruSummad (new int[][] { {1,2,3}, {4,5,6} }); // {5, 7, 9}
        // YOUR TESTS HERE

        System.out.println();
        for (int element : res){
            System.out.print(element + " ");
        }
    }

    public static int[] veeruSummad(int[][] m) {
        int max = Integer.MIN_VALUE;
        for (int[] row : m){
            if (row.length > max){
                max = row.length;
            }
        }

        int[] summad = new int[max];
        for (int i = 0; i < m.length; i++){
            for (int j = 0; j < m[i].length; j++){
                summad[j] += m[i][j];
            }
        }
        return summad;
    }

    public static void printMatrix(int[][] massiiv){
        //iga massiivi rea veergude kohta
        for (int x = 0; x < massiiv.length; x++ ){
            // iga massiivi veeru x elemendi kohta
            for(int y = 0; y < massiiv[x].length; y++){
                System.out.printf("%2d ", massiiv[x][y]);
            }
            System.out.println();
        }
    }
}