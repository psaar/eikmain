package Kontrolltoo2;

/**
 * Created by mmuru on 4.11.15.
 */
/*
Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse n korda n täisarvumaatriksi,
mille iga elemendi väärtuseks on maksimaalne selle elemendi reaindeksist ja veeruindeksist (indeksid algavad nullist).
 */
public class Muster2 {
    public static void main (String[] args) {
        int[][] res = muster (9);

        for (int[] row : res){
            for(int col : row){
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }

    public static int[][] muster (int n) {
        int[][] massiiv = new int[n][n];
        for(int i = 0; i < massiiv.length; i++){
            for(int j = 0; j < massiiv[i].length; j++){
                if(i > j){
                    massiiv[i][j] = i;
                }else{
                    massiiv[i][j] = j;
                }
            }
        }


        return massiiv;
    }
}