package Kontrolltoo2;

/**
 * Created by mmuru on 4.11.15.
 */
/*
Sportlase esinemist hindab n>2 kohtunikku. Hinnete hulgast eemaldatakse k�ige madalam ja
k�ige k�rgem ning leitakse �lej��nud n-2 hinde aritmeetiline keskmine. Kirjutada Java-meetod hinde arvutamiseks.
 */
public class ResultClass {

    public static void main (String[] args) {
        System.out.println (result (new double[]{0., 1., 2., 3., 4.}));
        // YOUR TESTS HERE
    }

    public static double result (double[] marks) {
        double minValue = Double.MAX_VALUE;
        int minIndex = 0;
        double maxValue = Double.MIN_VALUE;
        int maxIndex = 0;
        double resultSum = 0;

        for(int i = 0; i < marks.length; i++){
            //min value
            if(marks[i] < minValue){
                minValue = marks[i];
                minIndex = i;
            }
            //max value
            if(marks[i] > maxValue){
                maxValue = marks[i];
                maxIndex = i;
            }
        }
        //sum
        for (int i = 0; i < marks.length; i++){
            if (i != minIndex && i != maxIndex){
                resultSum += marks[i];
            }
        }

        return resultSum / (marks.length - 2);
    }
}