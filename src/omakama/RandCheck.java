package omakama;

import java.util.*;

public class RandCheck {

	public static void main(String[] args) {

		// List<long> nums = new ArrayList<long>();
		int[] numbers = new int[1000000];

		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = ((int) (Math.random() * Math.pow(10, 8)));
		}

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < numbers.length; i++) {
			int key = numbers[i];
			if (map.containsKey(key)) {
				int occurrence = map.get(key);
				occurrence++;
				map.put(key, occurrence);
			} else {
				map.put(key, 1);
			}
		}
		Iterator iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			int key = (Integer) iterator.next();
			int occurrence = map.get(key);

			if (occurrence > 2) {
				System.out.println(key + " occur " + occurrence + " time(s).");
			}
		}
	}
}
