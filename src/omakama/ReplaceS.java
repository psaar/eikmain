package omakama;

public class ReplaceS {

	public static void main(String[] args) {
		String value = "This Is A Test";
		System.out.println(replaceLowerChar(value, '-'));
	}
	public static String replaceLowerChar(String string, char newChar) {
		StringBuilder sb = new StringBuilder(string);
		for (int index = 0; index < sb.length(); index++) {
		    char c = sb.charAt(index);
		    if (Character.isLowerCase(c)) {
		        sb.setCharAt(index, newChar);
		    }
		}
		return sb.toString();
	}
}
