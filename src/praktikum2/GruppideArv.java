package praktikum2;

import lib.TextIO;

public class GruppideArv {

	public static void main(String[] args) {

		System.out.println("Palun sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();

		System.out.println("Palun sisesta grupi suurus: ");
		int grupSuurus = TextIO.getlnInt();

		int gruppideArv = inimesteArv / grupSuurus;
		System.out.println(gruppideArv);
		
		int j22k = inimesteArv % grupSuurus;
		System.out.println("Üle jääb " + j22k + " inimest.");
	}

}
