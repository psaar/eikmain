package praktikum2;

import lib.TextIO;

public class Korrutis {

	public static void main(String[] args) {

		// double - komaga arvud, boolean - true/false
		// int arv1, arv2, korrutis;
		//
		System.out.println("Palun sisesta kaks arvu");
		//
		// arv1 = TextIO.getlnInt();
		// arv2 = TextIO.getlnInt();
		//
		// korrutis = arv1 * arv2;
		//
		System.out.println("Nende arvude korrutis on: " + TextIO.getlnInt() * TextIO.getlnInt());
	}

}
