package praktikum2;

import lib.TextIO;

public class SisestaNimi {
	public static void main(String[] args) {
		String nimi;
		String value1 = "Juku";

		System.out.println("Sisesta nimi: ");
		nimi = TextIO.getlnString();

		if (nimi.contains(value1))
			System.out.println(nimi + " on imelik!");

		else
			System.out.println(nimi + " on okei vend!");
	}
}