package praktikum3;

import lib.TextIO;

public class Cumlaude {

	public static void main(String[] args) {

		System.out.println("Sisesta keskmine hinne:");
		double keskmineHinne = TextIO.getlnDouble();

		while (keskmineHinne < 0 || keskmineHinne > 5) {
			System.out.println("Vigane hinne! Sisesta uuesti:");
			keskmineHinne = TextIO.getlnDouble();

		}

		System.out.println("Sisesta lõputöö hinne:");
		double loputoo = TextIO.getlnDouble();

		while (loputoo < 0 || loputoo > 5) {
			System.out.println("Vigane hinne! Sisesta uuesti:");
			loputoo = TextIO.getlnDouble();
		}

		if (keskmineHinne > 4.5 && loputoo == 5) {
			System.out.println("Jah saad cum laude diplomile.");

		} else {
			System.out.println("Ei saa!");
		}
	}

}
