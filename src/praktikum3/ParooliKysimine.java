package praktikum3;

import lib.TextIO;

public class ParooliKysimine {

	public static void main(String[] args) {

		String oigeParool = "Saladus";

		System.out.println("Palun sisesta parool");
		String kasutajaSisestus = TextIO.getlnString();

		if (oigeParool.equals(kasutajaSisestus)) {
			System.out.println("Parool on õige");
		} else {
			System.out.println("Vale parool");
		}
	}

}
