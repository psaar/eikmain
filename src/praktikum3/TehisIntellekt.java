package praktikum3;

import lib.TextIO;

public class TehisIntellekt {

	public static void main(String[] args) {

		System.out.println("Palun sisesta kaks vanust");
		int vanus1 = TextIO.getInt();
		System.out.println("Sisesta teine vanus");
		int vanus2 = TextIO.getInt();

		int vanusevahe = Math.abs(vanus1 - vanus2);

		if (vanusevahe >= 5 && vanusevahe <= 10) {
			System.out.println("Suurem kui 5a");
		} else if (vanusevahe > 10) {
			System.out.println("Suurem kui 10a");
		} else
			System.out.println("Alla 5a");
	}

}
