package praktikum4;

import lib.TextIO;

public class Tabel {

	public static void main(String[] args) {

		System.out.print("Sisesta soovitud tabelis suurus: ");
		int tabeliSuurus = TextIO.getlnInt();
		
		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if (i == j) {
					System.out.print("1 ");
				} else {
					System.out.print("0 ");
				}
//				System.out.print("(i=" + i + "j=" + j + ")");
			}
			System.out.println();
		}
	}
}