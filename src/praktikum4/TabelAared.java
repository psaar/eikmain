package praktikum4;

import lib.TextIO;

public class TabelAared {

	public static void main(String[] args) {

		System.out.print("Sisesta soovitud tabelis suurus: ");
		int tabeliSuurus = TextIO.getlnInt();
		
		for (int i = 0; i < tabeliSuurus * 2 + 3; i++) {
			System.out.print("-");
		}
		System.out.println();
		
		
		for (int i = 0; i < tabeliSuurus; i++) {
			System.out.print("| ");
			for (int j = 0; j < tabeliSuurus; j++) {
				if (i == j || i + j == tabeliSuurus - 1) {
					System.out.print("x ");
				} else {
					System.out.print("0 ");
				}
//				System.out.print("(i=" + i + "j=" + j + ")");
			}
			System.out.println("|");
		}
		
		for (int i = 0; i < tabeliSuurus * 2 + 3; i++) {
			System.out.print("-");
		}
		System.out.println();
	}
}