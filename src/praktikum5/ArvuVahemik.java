package praktikum5;

import lib.TextIO;

public class ArvuVahemik {

	public static void main(String[] args) {
		kasutajaSisestus(0, 50);
	}

	public static int kasutajaSisestus(int min, int max) {
		
		int kasutajaSisestas;
		do {
			System.out.println("Palun sisesta arv vahemikus " + min + " kuni " + max);
			kasutajaSisestas = TextIO.getlnInt();
		
		} while (kasutajaSisestas < min || kasutajaSisestas > max);
		
		
		return max;
	}

	public static void kasutajaSisestus(String string, int i, int j) {
		kasutajaSisestus(i, j);
		
	}
}