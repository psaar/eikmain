package praktikum6;

import lib.TextIO;

public class Arvamine {

	public static void main(String[] args) {

		int arvutiArv = suvalineArv(1, 100);
		System.out.println("Sisesta arv 1 kuni 100" + arvutiArv);
		int userInput = TextIO.getlnInt();

		while (true) {
			if (arvutiArv > userInput) {
				System.out.println("Otsitav arv on suurem pakutud arvust, proovi uuesti!");
				userInput = TextIO.getlnInt();
			} else if (arvutiArv < userInput) {
				System.out.println("Otsitav arv on väiksem pakutud arvust, proovi uuesti!");
				userInput = TextIO.getlnInt();
			} else {
				System.out.println("Arvasid ära arvuti mõeldud arvu!");
				break;
			}
		}
	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;
	}

}
