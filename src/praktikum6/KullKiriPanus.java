package praktikum6;

import praktikum5.ArvuVahemik;

public class KullKiriPanus {

	public static void main(String[] args) {

		int userCash = 100;
		System.out.println("Sul on " + userCash + "$");

		while (userCash > 0) {
			System.out.println("Sisesta panus kuni 25$");
			int bet = ArvuVahemik.kasutajaSisestus(1, 25);
			
			int headsTails = Arvamine.suvalineArv(0, 1);

			if (headsTails == 0) {
				System.out.println("Võitsid");
				userCash += bet;
			} else {
				System.out.println("Kaotasid");
				userCash -= bet;
			}
			System.out.println("Sul on nüüd " + userCash + "$");
		}
	}
}
