package praktikum7;

/**
 * Created by mmuru on 16.10.15.
 */
public class MaximumElement {
    public static void main(String[] args){
        int[] array1 = {1,5,3,8,4,88,15,4,7};
        System.out.println("Esimese massiivi max v22rtus on: " + getMaxValueOf(array1));

        int[][] array2 = {
                {1, 5, 2, 6},
                {6, 4, 9, 15},
                {19, 5, 8, 3}
        };
        System.out.println("Teise massiivi max v22rtus on: " + getMaxValueOf(array2));
    }

    public static int getMaxValueOf(int[] array){
        int maxValue = 0;
        for (int i = 0; i < array.length; i++){
            if (array[i] > maxValue){
                maxValue = array[i];
            }
        }
        return maxValue;
    }

    public static int getMaxValueOf(int[][] array){
        int maxValue = 0;
        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array.length; j++){
                if (array[i][j] > maxValue){
                    maxValue = array[i][j];
                }
            }
        }
        return maxValue;
    }
}