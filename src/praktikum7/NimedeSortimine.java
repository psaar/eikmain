package praktikum7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by mmuru on 16.10.15.
 */
public class NimedeSortimine {
    public static void main(String[] args){
        //loon arraylisti
        ArrayList<String> names = new ArrayList<String>();
        //loon sk2nneri
        Scanner userInput = new Scanner(System.in);
        String _userInput = "empty";
        //kysin nimesid
        System.out.println("Sisesta nimesid: ");

        // <stringinimi>.isEmpty() kontrollib, kas string on tyhi
        while (!_userInput.isEmpty()){

            //stringi puhul pole try/catch kuigi oluline, sest k6ik sisestatud symbolid sobivad stringiks
            _userInput = userInput.nextLine();

            //kui polnud tyhi string, lisa nimi arraylist-i
            if (!_userInput.isEmpty()){
                names.add(_userInput);
            }
        }

        //sordin nimed
        Collections.sort(names);

        //prindin nimed
        for (int i = 0; i < names.size(); i++){
            System.out.println(names.get(i));
        }

        // sulgen sk2nneri
        userInput.close();
    }
}