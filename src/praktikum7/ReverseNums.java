package praktikum7;

import java.util.Scanner;

/**
 * Created by mmuru on 16.10.15.
 */
public class ReverseNums {
    public static void main(String[] args){
        // loon sk2nneri
        Scanner userInput = new Scanner(System.in);
        //loon arvude massiivi
        int[] userData = new int[10];

        //kysi 10 arvu
        for (int i = 0; i < 10; i++){
            try{
                System.out.println("Sisesta " + (i + 1) + ". arv");
                userData[i] = userInput.nextInt();
            }
            catch (Exception e) {
                System.out.println("See pole arv!!");
                break;
            }

        }

        //prindi v2lja tagurpidi
        System.out.println("Prindin v2lja tagurpidi:");
        for (int i = 10; i > 0; i--){
            // massiivi index yhe v6rra v2iksem kuna massiivi indexid on 0-9 mitte 1-10
            System.out.println(userData[i-1]);
        }
        // sulen sk2nneri
        userInput.close();
    }
}