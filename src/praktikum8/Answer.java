package praktikum8;

public class Answer {

	public static void main(String[] args) {
		int[][] res = muster(9);
		for (int[] row : res) {
			for (int col : row) {
				System.out.printf("%3d ", col);
			}
			System.out.println();
		}
	}

	public static int[][] muster(int n) {
		int[][] res = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				res[i][j] = (int) Math.min(i, j);
			}

		}
		return res;
	}
}