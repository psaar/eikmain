package praktikum8;

public class KeskmisestParemaid {

	   public static void main (String[] args) {
	      System.out.println (keskmisestParemaid (new double[]{0.5, 5.4, 4.2, 4.2, 2.4, 2.9, 1.2, 2.8, 9.4, 7.4, 5.5}));
	   }

	   public static int keskmisestParemaid (double[] d) {
		   double keskmine = 0;
		   int hulk = 0;
		   for (int i = 0; i < d.length; i++) {
			keskmine += d[i];
		}
		   keskmine = keskmine / d.length;
		   
		   for (int i = 0; i < d.length; i++) {
			if(d[i] > keskmine){
				hulk++;
			}
		}
		   return hulk;
	   }
	}