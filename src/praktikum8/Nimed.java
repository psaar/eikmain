package praktikum8;

import java.util.*;

import lib.TextIO;

public class Nimed {

	public static void main(String[] args) {

		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();

		while (true) {
			System.out.println("Sisesta enne nimi siis vanus.");
			String nimi = TextIO.getlnString();
			if(nimi.equals("")){
				break;
			}
			int vanus = TextIO.getInt();
			Inimene keegi = new Inimene(nimi, vanus);
			inimesed.add(keegi);
		}
		for (Inimene inimene : inimesed) {
			// Java kutsub välja Inimene klassi toString() meetodi
			//System.out.println(inimene);
			inimene.tervita();
		}
		// for (int i = 0; i < inimesed.size(); i++) { //t66tab sama moodi aga
		// pikem
		// System.out.println(inimesed.get(i));
		// }
	}

}
