package praktikum8;

public class Score {

	public static void main(String[] args) {
		System.out.println(score(new int[] { 4, 1, 2, 3, 0 })); // 9
	}

	public static int score(int[] points) {
		int score = 0;
		int min1 = Integer.MAX_VALUE;
		int min1Index = 0;
		int min2 = Integer.MAX_VALUE;

		for (int i = 0; i < points.length; i++) {
			if (points[i] < min1) {
				min1 = points[i];
				min1Index = i;
			}
		}
		for (int i = 0; i < points.length; i++) {
			if (points[i] < min2 && i != min1Index) {
				min2 = points[i];
			}
		}
		for (int element : points) {
			if (element != min1 && element != min2) {
				score += element;
			}
		}
		return score;
	}
}