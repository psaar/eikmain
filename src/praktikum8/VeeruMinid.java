package praktikum8;

public class VeeruMinid {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		int[] res = veeruMinid(new int[][] { { 1, 2, 6 }, { 4, 5, 3 } }); // {1, 2, 3}
	}

	public static int[] veeruMinid(int[][] m) {
		int min_size = 0;

		for (int[] row : m) {
			if (row.length > min_size) {
				min_size = row.length;
			}
		}

		int[] miinimumid = new int[min_size];

		for (int i = 0; i < miinimumid.length; i++) {
			int min = Integer.MAX_VALUE;
			for (int x = 0; x < m.length; x++) {
				if (m[x].length > i && m[x][i] < min) {
					min = m[x][i];
				}
			}

			miinimumid[i] = min;

		}

		for (int el : miinimumid) {
			System.out.print(el + ", ");
		}
		System.out.println();

		return miinimumid;
	}
}
