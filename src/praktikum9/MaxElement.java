package praktikum9;

/**
 * Created by margus@workstation on 29.10.2015.
 */
public class MaxElement {
    public static void main(String[] args){
        int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
        System.out.println(maxValue(massiiv));
    }

    public static int maxValue(int[] massiiv){
        int max = Integer.MIN_VALUE;
        for(int el : massiiv){
            if(el > max){
                max = el;
            }
        }
        return max;
    }
}