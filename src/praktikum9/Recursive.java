package praktikum9;

/**
 * Created by margus@workstation on 29.10.2015.
 */
public class Recursive {
    public static void main(String[] args){

        System.out.println(astenda(5,3));
    }

    public static int astenda(int arv, int aste){
        if (aste != 1){
            return arv * astenda(arv, aste - 1);
        }else{
            return arv;
        }
    }
}
/*astenda(5,3) 
				returns 5*astenda(5,2) 
										returns 5*5*5
*/