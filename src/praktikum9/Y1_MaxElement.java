package praktikum9;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by margus@workstation on 29.10.2015.
 */
public class Y1_MaxElement {

    public static Scanner userInput = new Scanner(System.in);

    public static void main(String[] args){
        ArrayList<Integer> numbers = new ArrayList<>();
        int curInput = -1;

        //saa kasutajalt numbrid
        while(curInput != 0){
            System.out.print("Sisesta positiivne number: ");
            curInput = getInt();
            //kui number on nullist v2iksem:
            if(curInput < 0){
                System.out.print("Proovi uuesti: ");
            }//kui number on null
            else if (curInput == 0){
                System.out.println("Katkestan");
            }//aktsepteeritav number:
            else{
                numbers.add(curInput);
            }
        }

        printHorizontalTable(numbers);

        userInput.close();
    }

    /**
     * prindi tabel horisontaalselt
     * @param mas ArrayList massiiv
     */
    public static void printHorizontalTable(ArrayList<Integer> mas){
        int f = FindMaxValueLength(mas);
        for(int el : mas){
            System.out.format("\n%" + f + "d ", el);
            printX(el);
        }

    }


    /**
     * saa kasutajalt int tyypi arv
     * @return int
     */
    public static int getInt(){
        int input = -1;
        try{
            input = userInput.nextInt();
        }catch(InputMismatchException e){
            userInput.next();
        }
        return input;
    }

    /**
     * Tagasta massiivi suurima elemendi pikkus. mitu numbrikohta on arvus
     * @param massiiv
     * @return int
     */
    public static int FindMaxValueLength(ArrayList<Integer> massiiv){
        int max = Integer.MIN_VALUE;
        for(int el : massiiv){
            if(el > max){
                max = el;
            }
        }
        return String.valueOf(max).length();
    }

    /**
     * prindi antud arv "x" m2rke
     * @param count "x"-ide arv
     */
    public static void printX(int count){
        for(int i = 0; i < count; i++){
            System.out.print("x");
        }
    }
}