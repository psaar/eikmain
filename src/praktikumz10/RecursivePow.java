package praktikumz10;

public class RecursivePow {

	public static void main(String[] args) {
		long i = 0;
		while (true) {
			System.out.println(i + " - " + fibonacchi(i));
			i++;
		}

	}

	public static int astenda(int arv, int aste) {
		if (aste == 0) {
			return 1;
		} else {
			return arv * astenda(arv, aste - 1);
		}
	}

	public static long fibonacchi(long n) {
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return fibonacchi(n - 1) + fibonacchi(n - 2);
		}
	}
}