package praktikumz11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

/**
 * Ringjoone valemi järgi ringi joonistamise näide
 * @author Mikk Mangus
 */
@SuppressWarnings("serial") //ei näita kollaseid hüüumärgi vigu
public class Ring extends Applet {

    private Graphics g;
    
    @Override
    public void paint(Graphics g) {
        this.g = g;
        joonistaTaust();
        joonistaRing();
    }
    
    /**
     * Katab tausta valgega
     */
    public void joonistaTaust() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
    
    /**
     * Joonistab ringi
     */
    public void joonistaRing() {
        g.setColor(Color.black);
        int keskkohtX = getWidth() / 2;
        int keskkohtY = getHeight() / 2;
        int raadius = 0;
        
        boolean esimene = true;
        int eelmineX = 0, eelmineY = 0;
        for (double nurk = 0; nurk <= Math.PI * 20; nurk = nurk + .25) {
            int x = (int) (raadius * Math.cos(nurk));
            int y = (int) (raadius * Math.sin(nurk));
//            g.fillRect(keskkohtX + x, keskkohtY + y, 2, 2);
            raadius++;
            
			if (esimene) {
				esimene = false;
			} else {
				g.drawLine(keskkohtX + x, keskkohtY + y, keskkohtX + eelmineX, keskkohtY + eelmineY);
			}
			eelmineX = x;
			eelmineY = y;
        }
    }
}