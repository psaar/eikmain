package praktikumz11;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Color;

@SuppressWarnings("serial")
public class Yleminek extends Applet {

	@Override
	public void paint(Graphics g) {
		// super.paint kutsuks välja just nimelt applet klassist .paint meetodi

		int w = getWidth();
		int h = getHeight();
		
		double colorChange = 255.0 / h;
		
		for (int i = 255; i >= 0; i--) {
			int colorCode = (int) (255 - i * colorChange);
			Color myColor = new Color(colorCode, colorCode, colorCode);
			g.setColor(myColor);
			g.drawLine(0, i, w, i);

		}
	}
}
