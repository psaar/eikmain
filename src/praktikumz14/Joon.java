package praktikumz14;

public class Joon {
	
	Punkt algPunkt, l6ppPunkt;

	public Joon(Punkt p1, Punkt p2){
		algPunkt = p1;
		l6ppPunkt = p2;
	}
	
	public double pikkus() {
		double a = l6ppPunkt.x - algPunkt.x;
		double b = l6ppPunkt.y - algPunkt.y;
		double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		return c;
//		return Math.sqrt(Math.pow((l6ppPunkt.x - algPunkt.x), 2) + Math.pow((l6ppPunkt.y - algPunkt.y), 2));
	}

	@Override
	public String toString() {
		return "Joon on " + algPunkt + " " + l6ppPunkt;
		
	}
}
