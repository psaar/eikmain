package praktikumz14;

public class Punkt {

	int x, y;
	
	public Punkt(int x, int y) { //konstruktormeetod, sama nimega mis klass, meetod erandlikult suure tähega
		this.x = x;
		this.y = y;
	}

	public Punkt() {}

	@Override
	public String toString() {
		return "Punkt on " + x + " " + y;
	}
}
