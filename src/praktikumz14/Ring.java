package praktikumz14;

public class Ring {
	
	Punkt keskPunkt;
	double raadius;
	
	public Ring(Punkt o, double r) {
		keskPunkt = o;
		raadius = r;
	}
	
	public double umberm66t() {
		return Math.PI * 2 * raadius;
		
	}
	public double pindala() {
		return Math.PI * Math.pow(raadius, 2);
	}
	
	public String toString() {
		return "Ringi ümbermõõt on " + umberm66t() + " ja pindala on " + pindala();
	}

}
