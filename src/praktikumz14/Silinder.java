package praktikumz14;

public class Silinder extends Ring {

	double k6rgus;
	
	public Silinder(Ring alus, double h) {
		super(alus.keskPunkt, alus.raadius);
		k6rgus = h;
	}
	
	private double kyljePind() {
		return k6rgus * umberm66t();
	}
	@Override
	public double pindala() {
		return 2 * super.pindala() + kyljePind(); //super et ei kutsuks lõputult samast klassist
	}
	
	public String toString() {
		return "Silindri pindala on " + pindala();
	}
}
