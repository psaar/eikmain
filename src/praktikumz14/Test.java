package praktikumz14;

public class Test {
	
	public static void main(String[] args) {
		
		Punkt minuPunkt = new Punkt();
		minuPunkt.x = 100;
		minuPunkt.y = 200;
		
		Punkt veelYks = new Punkt(200, 300);
		
		System.out.println(minuPunkt);
		System.out.println(veelYks);
		
		Joon minuJoon = new Joon(minuPunkt, veelYks);
		
		System.out.println(minuJoon.pikkus());
		
		Ring ring = new Ring(minuPunkt, 50);
		System.out.println(ring);
		
		Silinder minuSil = new Silinder(ring, 100);
		System.out.println(minuSil);
		
	}
}
