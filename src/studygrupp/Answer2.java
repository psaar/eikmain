package studygrupp;

public class Answer2 {

	/*
	 * Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal
	 * niisuguste elementide arvu, mis on rangelt suuremad kõigi elementide
	 * aritmeetilisest keskmisest (aritmeetiline keskmine = summa /
	 * elementide_arv).
	 * 
	 * Write a method in Java to find the number of elements strictly greater
	 * than arithmetic mean of all elements of a given array of real numbers d
	 * (arithmetic mean = sum_of_elements / number_of_elements).
	 */

	public static void main(String[] args) {
		System.out.println(keskmisestParemaid(new double[] { 0., 1., 2., 3., 4. }));
		// YOUR TESTS HERE
	}

	public static int keskmisestParemaid(double[] d) {

		int arve = 0;
		double keskmine = 0;
		double summa = 0;

		for (int i = 0; i < d.length; i++) {

			summa += d[i];

		}

		keskmine = summa / d.length;

		for (int i = 0; i < d.length; i++) {
			if (d[i] > keskmine) {
				arve++;
			}

		}
		return arve; // YOUR PROGRAM HERE
	}

}