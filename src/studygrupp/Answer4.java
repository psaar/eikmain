package studygrupp;

import java.util.Arrays;

public class Answer4 {

	/*
	 * Koostage Java meetod etteantud täisarvumaatriksi m reamaksimumide
	 * massiivi leidmiseks (massiivi i-s element on maatriksi i-nda rea suurima
	 * elemendi väärtus). Read võivad olla erineva pikkusega.
	 * 
	 * Write a method in Java to find the array of maximums of rows of a given
	 * matrix of integers m (i-th element of the answer is the maximum of
	 * elements of the i-th row in the matrix). Rows might be of different
	 * length.
	 */

	public static void main(String[] args) {
		int[] res = reaMaxid(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } });
		System.out.println(Arrays.toString(res));// {3, 6}
		// YOUR TESTS HERE
	}

	public static int[] reaMaxid(int[][] m) {

		int[] max = new int[m.length];

		for (int i = 0; i < m.length; i++) {
			max[i] = m[i][0];

			for (int j = 0; j < m[i].length; j++) {
				if (max[i] < m[i][j]) {
					max[i] = m[i][j];
				}

			}

		}
		return max;
	}

}