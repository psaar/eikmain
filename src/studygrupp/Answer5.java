package studygrupp;

public class Answer5 {

	/*
	 * On antud hinnete maatriks (int[][] g), milles on iga üliõpilase jaoks üks
	 * rida, mille elementideks on selle üliõpilase hinded (skaalal 0 kuni 5).
	 * Koostada Java meetod üliõpilaste pingerea moodustamiseks, mis tagastaks
	 * reanumbrite massiivi (kõrgeima keskhindega reast allapoole, võrdsete
	 * korral jääb ettepoole see rida, mille number on väiksem).
	 * 
	 * Matrix of grades (int[][] g) contains one row per student where elements
	 * of the row are grades (in scale 0 to 5) of corresponding student. Write a
	 * Java method to calculate an array of row indices in descending order of
	 * average grade, in case of equal averages the row that has smaller index
	 * comes first.
	 */

	public static void main(String[] args) {
		int[][] grades = new int[][] { { 5, 3, 1 }, { 4, 3, 5 } };
		int[] res = sortByAvg(grades); // {1,0}
		for (int i = 0; i < res.length; i++) {
			System.out.print(res[i] + " ");
		}
	} // main

	public static int[] sortByAvg(int[][] g) {
		return null;

	}
}