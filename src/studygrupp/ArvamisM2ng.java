package studygrupp; //KATKINEKOOD

import java.util.InputMismatchException;
import java.util.Scanner;

public class ArvamisM2ng {

	public static void main(String[] args) {
		int suvalineArv = (int) (Math.random() * 10);
		System.out.println(suvalineArv);
		
		int kasutajaSisestus = 0;

		while(kasutajaSisestus != suvalineArv) {
			kasutajaSisestus = userInput(1, 10);
		}
	}

	public static int userInput(int min, int max) {
		Scanner kasutajaSisestusScanner = new Scanner(System.in);
		int sisestus = min - 1;

		while ((sisestus < min) || (sisestus > max)) {
			try {
				sisestus = kasutajaSisestusScanner.nextInt();
				if(sisestus < min){
					System.out.println("Liiga v2ike arv");
				} else if(sisestus > max){
					System.out.println("Liiga suur arv");
				}
			} catch (InputMismatchException e) {
				System.out.print("Vigane sisestus ");
				System.out.println(e);
			}
		}
		kasutajaSisestusScanner.close();
		return sisestus;
	}
}