package studygrupp;

public class KT1yl {

	public static void main(String[] args) {

		System.out.println(nullideArv(new int[]{1,3,0,5,0,6,0}));
		//sama mis �lemine rida ainult pikemalt
		int[] massiiv = new int[]{1,3,0,5,0,6,0};
		int nulleMassiivis = nullideArv(massiiv);
		System.out.println(nulleMassiivis);
	}
	
	public static int nullideArv(int[] m){
		int nullideHulk = 0; //hoiab endas arvu kui palju on etteantud massiivis nulle
		for (int i = 0; i < m.length; i++) {
			//kas massiivi m i element on null
			if(m[i] == 0){
				//kui on liidame nullide hulgale 1 juurde
				nullideHulk++;
			}
		}
		//tagastan main meetodisse nullide hulga
		return nullideHulk;
	}
	public static int nullideArv2(int[] m){
		int nullideHulk = 0;
		
		for(int element : m){ //element on lihtsalt selle ts�kli sees kasutatud muutuja
			if(element == 0){
				nullideHulk++;
			}
		}
		return nullideHulk;
	}
}
