package studygrupp;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Kahem66tmelisedMassiivid {

	public static Scanner userInput = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int rowSize = 0;//mitu rida on maatriksis
		int colSize = 0;//mitu veergu on maatriksis
		
		System.out.println("Mitu rida on maatriksis? :");
		while(rowSize == 0) {
			rowSize = getInt();
		}
		
		System.out.println("Mitu veergu on maatriksis? :");
		while(colSize == 0) {
			colSize = getInt();
		}
		//defineerin massiivi
		int[][] matrix = new int[rowSize][colSize];
		//matrixi elemendid saavad v22rtused, mille genereerib generateMatrix
		matrix = generateMatrix(rowSize, colSize);
		//prindime
		printMatrix(matrix);
		
		userInput.close();
	}//tagastab kahem22tmelise massiivi. sisse: rowSize ja colSize
	
	public static void printMatrix(int[][] massiiv) {
		//iga massiivi rea veergude kohta
		for (int x = 0; x < massiiv.length; x++) {
			//iga massiivi veeru x elemendi kohta
			for (int y = 0; y < massiiv[x].length; y++) {
				System.out.printf("%2d ", massiiv[x][y]);
			}
			System.out.println();//reavahetus
		}
	}
	
	public static int [][] generateMatrix(int row, int col){
		int[][] matrix = new int [row][col];
		//ts8kkel iga rea kohta
		for(int x = 0; x < row; x++) {
			//ts8kkel iga veeru elemendi kohta
			for (int y = 0; y < col; y++) {
				//element saab endale random v22rtuse vahemikus 1-99
				matrix[x][y] = (int) (Math.random() * 100);
			}
		}
		return matrix;
	}

	public static int getInt() {
		int num = 0;
		try {
			num = userInput.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Error!" + e);
			userInput.next(); // t8hjendan puhvri
		}

		return num;
	}//scanner end

}//class end
