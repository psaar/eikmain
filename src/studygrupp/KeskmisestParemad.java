package studygrupp;

public class KeskmisestParemad {

	public static void main(String[] args) {
		System.out.println(keskmisestParemaid(new double[] { 0.5, 1.8, 9.5, 5.4, 3.6, 10.2 }));
		// YOUR TESTS HERE
	}

	public static int keskmisestParemaid(double[] d) {
		double keskmine = 0;
		double summa = 0;
		int i = 0;

		for (i = 0; i < d.length; i++) {
			summa = summa + d[i];
		}
		keskmine = summa / d.length;
		int suuremaid = 0;
		for (int j = 0; j < d.length; j++) {
			if (d[j] > keskmine) {
				suuremaid++;
			}
		}
		return suuremaid;
	}
}