package studygrupp;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Massiivid {

	public static Scanner userInput = new Scanner(System.in);

	public static void main(String[] args) {

		int massiiviSuurus = 0;
		System.out.println("Kui suurt massiivi soovid?");
		
		//k�si kasutaja k�est kuni sisestab mingi arvu
		while(massiiviSuurus <= 0) {
			massiiviSuurus = getInt();
		}
		
		int[] massiiv = new int[massiiviSuurus];
		
		System.out.println(); //ilu p�rast
		for (int i = 0; i < massiiviSuurus; i++) {
			System.out.println("Sisesta " + (i + 1) + ". number: ");
			//k�si kasutaja k�est kuni sisestab mingi arvu
			while(massiiv[i] == 0) {
				massiiv[i] = getInt();//saan kasutajalt sisestuse	
			}
		}
		System.out.println(); //ilu p�rast
		
		//iga massiivi"massiiv" elemendi "massiiviElement" tee seda:
		for(int massiiviElement : massiiv) {
			System.out.println(massiiviElement + " ");
		}
		userInput.close();
	}

	public static int getInt() {
		int num = 0;
		try{
			num = userInput.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Error!" + e);
			userInput.next(); //t�hjendan puhvri
		}
		
		return num;
	}
}
