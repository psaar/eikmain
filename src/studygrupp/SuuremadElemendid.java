package studygrupp;

public class SuuremadElemendid {

	public static void main(String[] args) {
		int[] massiiv = new int[10];
		//t�idan juhuarvudega
		for (int i = 0; i < massiiv.length; i++) {
			massiiv[i] = (int) (Math.random() * 10);
			System.out.println(massiiv[i] + " ");
		}
		
		//elementide summa leida
		//leida keskmine
		double keskmine = leiaKeskmine(massiiv);
		System.out.println("Keskmine: " + keskmine);
		//v�rrelda elemente
		int suuremateArv = suuremKeskmisest(massiiv, keskmine);
		
		System.out.println("Keskmisest suuremaid elemente: " + suuremateArv);
	}
	public static int suuremKeskmisest(int[] massiiv, double keskmine) {
		int hulk = 0;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > keskmine) {
				hulk++;
			}
		}
		return hulk;
	}
	
	public static double leiaKeskmine(int[] massiiv) {
		double elementideSumma = 0;
		//leiame elementide summa
		for (int i = 0; i < massiiv.length; i++) {
			//olemasolevale massiivi v��rtusele lisan massiiv[i] v��rtuse
			elementideSumma += massiiv[i];
		}
		
		return elementideSumma / massiiv.length;
	}
}
