package studygrupp;

public class Tellis {
	   
	   public static void main (String[] param) 
	   {
		   System.out.println(mahub(1, 9, 3, 4, 5));
	   }

	   public static boolean mahub (double a, double b, double c, double x, double y) 
	   {
	      /*
	       * erinevad tahud on ab, bc ja ac. 
	       * saab lahendada ka �HE pika IF lausega
	       */
		   if((a <= x && b <= y) || (a <= y && b <= x)){
			   // k�lg ab mahub l�bi
			   return true;
		   }
		   else if((c <= x && b <= y) || (c <= y && b <= x)){
			   // k�lg bc mahub l�bi
			   return true;
		   }
		   else if((a <= x && c <= y) || (a <= y && c <= x)){
			   // k�lg ac mahub l�bi
			   return true;
		   }
		   else return false;

	   }
	} 