package studygrupp;

public class Transponse {

	public static void main(String[] args) {
		
		int rowSize = 0;//mitu rida on maatriksis
		int colSize = 0;//mitu veergu on maatriksis
		
		System.out.println("Mitu rida on maatriksis? :");
		while(rowSize == 0) {
			rowSize = Kahem66tmelisedMassiivid.getInt();
		}
		
		System.out.println("Mitu veergu on maatriksis? :");
		while(colSize == 0) {
			colSize = Kahem66tmelisedMassiivid.getInt();
		}
		//defineerin maatriksi
		int[][] maatriks = new int[rowSize][colSize];
		maatriks  = Kahem66tmelisedMassiivid.generateMatrix(rowSize, colSize);
		Kahem66tmelisedMassiivid.printMatrix(maatriks);
		
		int[][] maatriks2 = new int[colSize][rowSize];
		
		for (int row = 0; row < rowSize; row++) {
			for (int col = 0; col < colSize; col++) {
				maatriks2[col][row] = maatriks[row][col];
			}
		}
		System.out.println();
		Kahem66tmelisedMassiivid.printMatrix(maatriks2);
		
		twoMin(maatriks);
	}
	
	public static void twoMin(int[][] massiiv) {
		//miinimumid saavad maksimaalse t2isarvu v22rtuse, mida int saab omada
		int min1 = Integer.MAX_VALUE;
		int min2 = Integer.MAX_VALUE;
		//iga massiivi rea kohta tee seda:
		for(int[] row : massiiv) {
			//iga massiivi rea veeru elemendi kohta tee seda:
			for(int col : row) {
				if(col < min1) {
					min1 = col;
				} else if(col < min2 && col != min1) {
					min2 = col;
				}
			}
		}
		System.out.println("Min v22rtused: " + min1 + " " + min2);
	}//endOF twoMin
}
